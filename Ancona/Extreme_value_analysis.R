# Extreme value analysis

# Environment
rm(list=ls())

# Library
library(ismev)
library(extRemes)
library(dplyr)

# Data 
data <- read.csv("Data/Ancona/Ancona_sea_level.csv") %>%
  dplyr::select(-X)
data$DateTime <- as.POSIXct(data$DateTime, tz="UTC")
data2 <- read.csv("Data/Ancona/Ancona_sea_levelr.csv") %>%
  dplyr::select(-X)
data2$DateTime <- as.POSIXct(data2$DateTime, tz="UTC")

####################################
######## Block maxima ###############
#####################################

####  GEV model #######

## We first define an appropriate time window to use in maxima building (maxima should be iid) ####

# Month
dati.month <- data2 
dati.month$DateTime <- format(dati.month$DateTime, format = "%Y-%m")

dati.maxi.m <- dati.month %>%
  group_by(DateTime) %>%
  slice(which.max(levelr))

# Autocorrelation
acf(dati.maxi.m$levelr)



## We estimate the GEV model ####


# GEV: MLE
dati.gev.fit <- gev.fit(dati.maxi.m$levelr)
# The convergence output tells us that the numerical maximization of the likelihood was successful.
# We check now if the fit using the gev distribution is sensible by running a diagnostic:
gev.diag(dati.gev.fit) 
# We obtain 4 outputs:
# 1. Probability plot: we plot model estimated probabilities against empirical ones ;
# 2. QQplot compare the observed quantiles to the theoretical ones ;
# 3. Return level plot: we plot the estimated return levels of order N 
# against a range of N-values with their asymptotic confidence intervals ;
# 4. Density plot: observed histogram and GEV curve on one plot.

# Fo the first 2 plots, the model seems appropriate because the points are well 
# following the diagonal line.
# The return levels appear good estimated, staying in the confidence interval.
# Finally, the GEV curve seems to follow (same distribution) the observed histogram.


# We also want to explore the case of non stationarity:

zdat <- matrix(1:length(dati.maxi.m$levelr),ncol=1) #def matrix for time
dati.trend.fit <- gev.fit(dati.maxi.m$levelr,zdat,mul=1)
gev.diag(dati.trend.fit) # no return level (have to project the time covariate)
# We now compute a Likelihood ratio tests to verify the hypothesis: is the GEV distribution 
# better if we consider it with or without stationarity.
lik.ratio <- 2*(dati.gev.fit$nllh-dati.trend.fit$nllh)
print(1-pchisq(lik.ratio, 1)) #pvalue no evidence in favor of the trend model
# We will stay with the stationary model.



## We compute return levels of order 2, 10, 20 e 50 ####


# To compute the return levels, we use the Maximum Likelihood Estimation of the parameters:
# location, scale and shape, that we obtained when we fitted the GEV distribution.


rlevd(c(2,10,22,50), loc = dati.gev.fit$mle[1], scale = dati.gev.fit$mle[2], 
      shape = dati.gev.fit$mle[3], type = "GEV", npy=365.25)

# The returns levels told us: 
# For 22 years: with probability 1/22=0.045, the value of the quantile is 0.6232785

plot.ts(data$level)
abline(h = 0.6232785 , col = "red") # extreme line for sea level and not residual sea levels
abline(h = -0.6232785 , col = "red")

data.extr <- data2 %>%
  filter(levelr < 0.6232785  & levelr > -0.6232785 )
plot.ts(data.extr$level)
