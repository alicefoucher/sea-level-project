# Environment
rm(list=ls())

# Library
library(SparkR)

# Initialize Spark Context
sparkR.session()

# Set Spark Context
sc <- sparkR.session()

# Load required packages
sparkR.session().conf$spark.sql.extensions <- "com.databricks.spark.avro"
library(dplyr)
library(tidyr)
library(stringr)
#install.packages("TideHarmonics")
library(TideHarmonics)
#install.packages("imputeTS")
library(imputeTS)
#install.packages("circular")
library(circular)
library(readr)
library(aws.s3)

aws.s3::get_bucket("id1867", region = "")
data <- 
  aws.s3::s3read_using(
    FUN = data.table::fread,
    object = "/Carloforte/Carloforte_sea_level.csv",
    bucket = "id1867",
    opts = list("region" = "")
  )

# Select columns
data <- SparkR::select(data, -X)

# Convert DateTime to POSIXct
data <- SparkR::withColumn(data, "DateTime", SparkR::from_unixtime(SparkR::unix_timestamp(data$DateTime, "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", tz = "UTC"))